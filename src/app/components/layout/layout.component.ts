import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public setName:any;
  public setMessage:any;
  public message:any ={
   showName:'',
   showMessage:'',
   showDate:'' ,
  };

  peopleInfo: any[] = [
  ]

  newMessage(){
    this.message.showName=this.setName
    this.message.showMessage=this.setMessage
    this.message.showDate=new Date();
    this.peopleInfo.push(this.message)
    this.setName='';
    this.setMessage='';

  }

  constructor() { }

  ngOnInit(): void {
  }

}
